# INSTALL_LOCATIONS = "/Users/shane/unity/Eye Shader/Assets/" \
# 	 "/Users/shane/unity/One Jar/Assets/" \
# 	 "/Users/shane/unity/procgen-jar-proj/Assets"

LIBRARIES = $(wildcard src/bin/Debug/netstandard2.0/*.dll)
.PHONY: all build clean test run benchmark

all: build

build:
	dotnet build

test:
	cd test && dotnet test

# benchmark:
# 	cd benchmark && dotnet run -c Release

clean:
	dotnet clean

# install: build
# 	for dir in $(INSTALL_LOCATIONS); do \
# 		cp $(LIBRARIES) "$$dir" || exit 1; \
# 	done

# doc:
# 	doxygen Doxyfile.txt
