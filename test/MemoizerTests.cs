// https://github.com/atomicobject/CSharpMemoizer/blob/master/Memoize/MemoizerTest.cs
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace SeawispHunter.Common.Tests {

public class DoThings {

  public int count0 = 0;
  public int Arity0() {
    return count0++;
  }

  public int count1 = 0;
  public int Arity1(int x) {
    return count0++ + x;
  }

  public int noInput = 0;
  private Func<string> _WithNoInput;
  public Func<string> WithNoInput
    => _WithNoInput ?? (_WithNoInput = Memoizer.Memoize(() => {
        noInput++;
      // Console.Out.WriteLine("Real WithNoInput method ran");
        return "the value";
        }));

  public int oneInput = 0;
  Func<string, string> _WithOneInput;
  public Func<string, string> WithOneInput
    => _WithOneInput ?? (_WithOneInput = Memoizer.Memoize(
                       (string x) => {
                         // Console.Out.WriteLine("Real WithOneInput method ran");
                         oneInput++;
                         return String.Format("Hello, {0}", x);
                       }));

  public int complexInput = 0;
  Func<string, Dictionary<string, string>, Dictionary<string, string>>
    _WithComplexInput;
  public Func<string, Dictionary<string, string>, Dictionary<string, string>>
    WithComplexInput
    => _WithComplexInput ?? (_WithComplexInput = Memoizer.Memoize(
                       (string x, Dictionary<string, string> y) => {
                         // Console.Out.WriteLine("Real WithComplexInput method ran");
                         complexInput++;
                         var result = new Dictionary<string, string>();
                         foreach (string key in y.Keys) {
                           result[key] = y[key] + x;
                         }
                         return result;
                       }));
}

public class TestIt {
  [Fact]
  public void TestArity0() {
    var doThings = new DoThings();
    Assert.Equal(0, doThings.Arity0());
    Assert.Equal(1, doThings.Arity0());
    Assert.Equal(2, doThings.Arity0());

    var doThings_Arity0 = Memoizer.Memoize<int>(new DoThings().Arity0);
    Assert.Equal(0, doThings_Arity0());
    Assert.Equal(0, doThings_Arity0());
    Assert.Equal(0, doThings_Arity0());
  }

  [Fact]
  public void The_no_input_case_only_runs_the_real_function_once() {
    // You should see only one "Real WithNoInput method ran" message in the console log
    // Console.Out.WriteLine("You should only see one 'Real WithNoInput method ran' message in the log below");
    var doThings = new DoThings();
    Assert.Equal(0, doThings.noInput);
    Assert.Equal("the value", doThings.WithNoInput());
    Assert.Equal(1, doThings.noInput);
    Assert.Equal("the value", doThings.WithNoInput());
    Assert.Equal(1, doThings.noInput);
    Assert.Equal("the value", doThings.WithNoInput());
    Assert.Equal(1, doThings.noInput);
  }

  [Fact]
  public void The_one_input_case_only_runs_the_real_function_once_when_the_input_is_the_same() {
    var doThings = new DoThings();

    // You should only see two "Real WithOneInput method ran" messages in the console log
    // Console.Out.WriteLine("You should only see two 'Real WithOneInput method ran' messages in the log below");
    Assert.Equal(0, doThings.oneInput);
    Assert.Equal("Hello, Dan.", doThings.WithOneInput("Dan."));
    Assert.Equal(1, doThings.oneInput);
    Assert.Equal("Hello, David.", doThings.WithOneInput("David."));
    Assert.Equal(2, doThings.oneInput);
    Assert.Equal("Hello, David.", doThings.WithOneInput("David."));
    Assert.Equal(2, doThings.oneInput);
    Assert.Equal("Hello, Dan.", doThings.WithOneInput("Dan."));
    Assert.Equal(2, doThings.oneInput);
  }

  [Fact]
  public void The_complex_input_case_only_runs_the_real_function_once_when_the_input_is_the_same() {
    var doThings = new DoThings();

    // You should see two "Real WithComplexInput method ran" messages in the console log
    // Console.Out.WriteLine("You should only see two 'Real WithComplexInput method ran' messages in the log below");

    var expected = new Dictionary<string, string>() {{"one", "1-digit"}, {"two", "2-digit"}};
    var input = new Dictionary<string, string>() {{"one", "1"}, {"two", "2"}};
    var append = "-digit";

    Assert.Equal(0, doThings.complexInput);
    Assert.Equal(expected, doThings.WithComplexInput(append, input));
    Assert.Equal(1, doThings.complexInput);
    Assert.Equal(expected, doThings.WithComplexInput(append, input));
    Assert.Equal(1, doThings.complexInput);
    Assert.Equal(expected, doThings.WithComplexInput(append, input));
    Assert.Equal(1, doThings.complexInput);

    var doThings2 = new DoThings();
    Assert.Equal(0, doThings2.complexInput);
    Assert.Equal(expected, doThings2.WithComplexInput(append, input));
    Assert.Equal(1, doThings2.complexInput);
    Assert.Equal(expected, doThings2.WithComplexInput(append, input));
    Assert.Equal(1, doThings2.complexInput);
  }
}

}
