using System;
using System.Linq;
using System.Collections.Generic;

namespace SeawispHunter.Common {

public class CommonException : Exception {
  public CommonException() { }
  public CommonException(string msg) : base(msg) { }
  public CommonException(string msg, Exception inner) : base(msg, inner) { }
}

public static class CommonExtensions {

  private static Random random => RandomFactory.GetRandom();
  //$ cite -ma 21351230 -t excerpt -u \
  // http://stackoverflow.com/questions/3173718/how-to-get-a-random-object-using-linq


  /** I used to like doing it like this: Grab a random item from any
      enumerable. Unfortunately, it may instantiate a list.

      ```
      public static T Random<T>(this IEnumerable<T> enumerable) {
        var list = enumerable as IList<T> ?? enumerable.ToList();
        return list.Random();
      }
      ```

      New rule: If it ain't performant, don't make it convenient.
      -----------------------------------------------------------

      Rather than providing this for IEnumerable<T> which doesn't have a count
      and isn't indexable, let's provide it for IList<T> which is indexable and
      has a count. (Did you know T[] is an IList<T>? It is!)

      Imagine you had code like this:

      ```
      IEnumerable<T> items = GetItems();
      var inLeftHand = items.Random();  // Copy to list.
      var inRightHand = items.Random(); // Copy to list again.
      var inPocket = items.Random();    // Copy to list again!
      ```

      This'll allocate up three lists, which could be large. Better to forgo the
      convenience, and let the caller decide when they want to allocate a list.

      ```
      IList<T> items = GetItems().ToList(); // Or ToArray(). Works with either.
      var inLeftHand = items.Random();
      var inRightHand = items.Random();
      var inPocket = items.Random();
      ```

      Thanks to @itsaddis for the kind recommendation[1].

      [1]: https://twitter.com/itsaddis/status/1242043608989151232
  */
  // public static T Random<T>(this IEnumerable<T> enumerable) {
  //   var list = enumerable as IList<T> ?? enumerable.ToList();
  //   return list.Random();
  // }

  /** Return a random item from the list. Throws exception when list has no
      items. */
  public static T Random<T>(this IList<T> list) {
    int index;
    return list.RandomWithIndex(out index);
  }

  /** Return a random item from the list and its index. Throws exception when
      list has no items. */
  public static T RandomWithIndex<T>(this IList<T> list, out int index) {
    switch (list.Count) {
      case 0:
        throw new CommonException("List must contain something to pick a random item.");
      case 1:
        return list[index = 0];
      default:
        return list[index = random.Next(list.Count)];
    }
  }

  /** Grab a random item from the stream not knowing how big the stream is. Only
      iterates the stream once. This works even if your stream isn't countable
      unlike RandomWithIndex.

      It calls the random number generator once for every item it traverses.

      It's basically reservoir sampling specialized for k = 1.
   */
  public static T RandomFromStream<T>(this IEnumerable<T> enumerable, int maxLookUp) {
    if (maxLookUp < 0)
      throw new ArgumentOutOfRangeException(nameof(maxLookUp), maxLookUp, "Expected positive number.");

    using (var e = enumerable.GetEnumerator()) {
      if (! e.MoveNext())
        throw new CommonException("No item available from stream.");
      T item = e.Current;
      for (int i = 1; i < maxLookUp && e.MoveNext(); i++)
        if (random.Next(i + 1) == 0)
          item = e.Current;

      return item;
    }
  }

  public static bool TryRandomFromStream<T>(this IEnumerable<T> enumerable, out T result, int maxLookUp) {
    if (maxLookUp < 0)
      throw new ArgumentOutOfRangeException(nameof(maxLookUp), maxLookUp, "Expected positive number.");

    using (var e = enumerable.GetEnumerator()) {
      if (! e.MoveNext()) {
        result = default(T);
        return false;
      }
      T item = e.Current;
      for (int i = 1; i < maxLookUp && e.MoveNext(); i++)
        if (random.Next(i + 1) == 0)
          item = e.Current;

      result = item;
      return true;
    }
  }

  public static bool TryRandomFromStream<T>(this IEnumerable<T> enumerable,
                                            out T result,
                                            double ignoreLessThanProbability = 0.01) {
    if (ignoreLessThanProbability < 0.0 || ignoreLessThanProbability > 1.0)
      throw new ArgumentOutOfRangeException(nameof(ignoreLessThanProbability),
                                            ignoreLessThanProbability,
                                            "Expected range [0, 1].");
    return enumerable.TryRandomFromStream(out result, (int) Math.Ceiling(1.0 / ignoreLessThanProbability));
  }

  /** Grab a random item from a stream. Ignore items that have less than a
      certain probability of being included.

      For instance, if given 0.01 or 1% then it will consider 100 items from the
      stream.
  */
  public static T RandomFromStream<T>(this IEnumerable<T> enumerable,
                                      double ignoreLessThanProbability = 0.01) {
    if (ignoreLessThanProbability < 0.0 || ignoreLessThanProbability > 1.0)
      throw new ArgumentOutOfRangeException(nameof(ignoreLessThanProbability),
                                            ignoreLessThanProbability,
                                            "Expected range [0, 1].");
    return enumerable.RandomFromStream((int) Math.Ceiling(1.0 / ignoreLessThanProbability));
  }


  // Reservoir Sampling
  // https://en.wikipedia.org/wiki/Reservoir_sampling
  /*
    Return an IEnumerable with k random items from the given
    enumerable with n items.

    It's O(n) and doesn't need to get the count n first.

    Note: That if the enumerable only has k items, the result will not
    be shuffled.
  */
  public static IList<T> ReservoirSampling<T>(this IList<T> source, int k) {
    var list = new T[k];
    int n = source.Count;
    for (int i = 0; i < k; i++)
      list[i] = source[i];

    for (int j, i = k; i < n; i++) {
      j = random.Next(i);
      if (j < k)
        list[j] = source[i];
    }
    return list;

    // using (var e = source.GetEnumerator()) {
    //   for (int i = 0; i < k && e.MoveNext(); i++) {
    //     list.Add(e.Current);
    //   }
    //   for (int j, i = k + 1; e.MoveNext(); i++) {
    //     j = random.Next(i);
    //     if (j < k)
    //       list[j] = e.Current;
    //   }
    //   return list;
    // }
  }

  // Fisher-Yates-Durstenfeld shuffle
  //$ cite -ma 1653204 -t excerpt -u \
  // http://stackoverflow.com/questions/1651619/optimal-linq-query-to-get-a-random-sub-collection-shuffle

  public static void Shuffle<T>(this IList<T> source) {
    for (int i = 0; i < source.Count; i++) {
      int j = random.Next(i, source.Count);
      T tmp = source[i];
      source[i] = source[j];
      source[j] = tmp;
      // The IL is a little worse, but the generated assembly is the same.
      // (source[i], source[j]) = (source[j], source[i]);
    }
  }

  // [Obsolete("Use IList<T>.Shuffle(), works on T[] and List<T>.")]
  // public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> source) {
  //   var buffer = source.ToArray();
  //   for (int i = 0; i < buffer.Length; i++) {
  //     int j = random.Next(i, buffer.Length);
  //     yield return buffer[j];
  //     buffer[j] = buffer[i];
  //   }
  // }

  /** Given a set of enumerables return a value from each in turn. This ensures that
      an infinite enumerable will not constitute the whole result. */
  public static IEnumerable<T> ThreadEnumerables<T>(params IEnumerable<T>[] enumerables) {
    Queue<IEnumerator<T>> q = new Queue<IEnumerator<T>>(enumerables.Length);
    foreach (var e in enumerables)
      q.Enqueue(e.GetEnumerator());
    while (q.Any()) {
      var enumerator = q.Dequeue();
      if (enumerator.MoveNext()) {
        yield return enumerator.Current;
        q.Enqueue(enumerator);
      } else {
        // This enumerator is all done. Dispose of it.
        enumerator.Dispose();
      }
    }
  }

  /** Returns an infinite stream of random items.

      And makes copies the enumerable to a list on each iteration. AAHHHH! */
  // public static IEnumerable<T> RandomStream<T>(this IEnumerable<T> enumerable) {
  //   while (true)
  //     yield return enumerable.Random(); // FIX: Copy list EVERYTIME!
  // }

  /** Returns an infinite stream of random items with replacement. */
  public static IEnumerable<T> RandomStream<T>(this IList<T> list) {
    while (true)
      yield return list.Random();
  }

  /** Returns an infinite stream of random items drawn without replacement. Once
      all items have been drawn, they're "refilled".

      (Initially called this RandomWithoutReplacementStream() but yeah.) */
  public static IEnumerable<T> ShuffleStream<T>(this IList<T> list) {
    list = list.ToList();       // Copy list.
    while (true) {
      list.Shuffle();
      for (int i = 0; i < list.Count; i++)
        yield return list[i];
    }
  }

  // https://stackoverflow.com/questions/20036267/net-iterate-through-an-ienumerable-several-elements-at-a-time
  public static IEnumerable<IList<T>> ChunksOf<T>(this IEnumerable<T> sequence, int size) {
    List<T> chunk = new List<T>(size);
    foreach (T element in sequence) {
      chunk.Add(element);
      if (chunk.Count >= size) {
        yield return new List<T>(chunk);
        chunk.Clear();
      }
    }
  }

  // https://stackoverflow.com/questions/10297124/how-to-combine-more-than-two-generic-lists-in-c-sharp-zip
  public static IEnumerable<TResult>
    Zip3<T1, T2, T3, TResult>(this IEnumerable<T1> source,
                                   IEnumerable<T2> second,
                                   IEnumerable<T3> third,
                                   Func<T1, T2, T3, TResult> func) {
    using (var e1 = source.GetEnumerator())
      using (var e2 = second.GetEnumerator())
        using (var e3 = third.GetEnumerator())
          while (e1.MoveNext() && e2.MoveNext() && e3.MoveNext())
            yield return func(e1.Current, e2.Current, e3.Current);
  }

  /*
    Behaves like .Skip(count) unless that would skip all the elements, in which
    case it returns the last element.

    new int[] {0, 1, 2, 3, 4}.Skip(10) -> { }
    new int[] {0, 1, 2, 3, 4}.SkipUnlessLast(10) -> { 4 }

    But otherwise behaves the same.

    new int[] {0, 1, 2, 3, 4}.Skip(2) -> { 2, 3, 4 }
    new int[] {0, 1, 2, 3, 4}.SkipUnlessLast(2) -> { 2, 3, 4 }
   */
  public static IEnumerable<T> SkipUnlessLast<T>(this IEnumerable<T> source, int count) {
    // https://blogs.msmvps.com/jonskeet/2011/01/02/reimplementing-linq-to-objects-part-23-take-skip-takewhile-skipwhile/
    T lastItem = default(T);
    using (IEnumerator<T> iterator = source.GetEnumerator()) {
      for (int i = 0; i < count; i++) {
        if (! iterator.MoveNext()) {
          if (i != 0)
            yield return lastItem;
          yield break;
        } else {
          lastItem = iterator.Current;
        }
      }
      bool anyMore = false;
      while (iterator.MoveNext()) {
        anyMore = true;
        yield return iterator.Current;
      }
      if (! anyMore)
        yield return lastItem;
    }
  }

  // https://stackoverflow.com/questions/39484996/rotate-transposing-a-listliststring-using-linq-c-sharp
  public static IEnumerable<IEnumerable<T>> Pivot<T>(this IEnumerable<IEnumerable<T>> source) {
    var enumerators = source.Select(e => e.GetEnumerator()).ToArray();
    try {
      while (enumerators.All(e => e.MoveNext()))
        yield return enumerators.Select(e => e.Current).ToArray();
    } finally {
      Array.ForEach(enumerators, e => e.Dispose());
    }
  }

  // https://stackoverflow.com/questions/1019737/favorite-way-to-create-an-new-ienumerablet-sequence-from-a-single-value
  public static IEnumerable<T> Yield<T>(this T item) {
    yield return item;
  }

  public static IEnumerable<T> Yield<T>(this Func<T> generator) {
    yield return generator();
  }

  /** Repeat source as an infinite stream. (The contents may not actually repeat depending on source.) */
  public static IEnumerable<T> Cycle<T>(this IEnumerable<T> source) {
    while (source.Any())
      foreach (var e in source)
        yield return e;
  }

  public static Predicate<IEnumerable<F>> SoloPredicate<F>(this Func<F, bool> func) {
    return fs => fs.Select(f => func(f)).Any(x => x);
  }


  // public static IEvaluator<G, F3> Unzip<G, F, F2, F3>(this IEvaluator<G, F> e1, IEvaluator<G, F2> e2, Func<F, F2, F3> func) {
  //   return new EvaluatorFunc<G, F3>(genotypes => e1.Evaluate(genotypes).Zip(e2.Evaluate(genotypes), func));
  // }
}

}
