using System;
using System.Collections.Generic;

namespace SeawispHunter.Common {

public static class Memoizer {

  public static Func<TReturn> Memoize<TReturn>(Func<TReturn> func) {
    bool hasValue = false;
    TReturn cache = default(TReturn);
    return () => {
      if (! hasValue) {
        cache = func();
        hasValue = true;
      }
      return cache;
    };
  }

  public static Func<TSource, TReturn>
    Memoize<TSource, TReturn>(Func<TSource, TReturn> func) {
    var cache = new Dictionary<TSource, TReturn>();
    return s => {
      TReturn result;
      if (! cache.TryGetValue(s, out result))
        cache[s] = result = func(s);
      return result;
    };
  }

  /** This isn't fool proof. You will have collisions. */
  public static Func<TSource1, TSource2, TReturn>
    Memoize<TSource1, TSource2, TReturn>(Func<TSource1, TSource2, TReturn> func) {
    var cache = new Dictionary<int, TReturn>();
    return (s1, s2) => {
      var key = s1.GetHashCode() ^ s2.GetHashCode();
      TReturn result;
      if (! cache.TryGetValue(key, out result))
        cache[key] = result = func(s1, s2);
      return cache[key];
    };
  }

  // And the pattern should continue for TSource1..TSourceN
}

}
