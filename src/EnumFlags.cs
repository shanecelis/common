/*
  Copyright (c) 2016 Seawisp Hunter, LLC

  Author: Shane Celis
*/

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

namespace SeawispHunter.Common {

public static class EnumFlags {

  /** Return the number of flags set for a given enum value. */
  public static int FlagCount<T>(this T flags)
    where T : struct, IConvertible {
      Type type = typeof(T);
      if (type.IsEnum) {
        var values = Enum.GetValues(type);
        int state = Convert.ToInt32(flags);
        int count = 0;
        foreach (var enumValue in values) {
          int value = (int) enumValue;
          if ((state & value) == value)
            count++;
        }
        return count;
      } else {
        throw new System.Exception("Type " + type + " is not an Enum.");
      }
    }

  /** Return the flags set for a given enum value. */
  // XXX: Or just Flags().
  public static IEnumerable<T> GetFlags<T>(this T flags)
    where T : struct, IConvertible {
    Type type = typeof(T);
    if (type.IsEnum) {
      var values = Enum.GetValues(type);
      int state = Convert.ToInt32(flags);
      foreach (var enumValue in values) {
        int value = (int) enumValue;
        if ((state & value) == value)
          yield return (T) Enum.ToObject(type, value);
      }
    } else {
      throw new System.Exception("Type " + type + " is not an Enum.");
    }
  }

  // XXX: A little weirdly specific form of a ToString().
  public static string ToNames<T>(this T flags)
    where T : struct, IConvertible {
    var matchedNames = flags.GetFlags().Select(x => x.ToString());
    return "(" + string.Join("|", matchedNames.ToArray()) + ")";
  }
}
}
