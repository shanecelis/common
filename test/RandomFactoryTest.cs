using System;
using Xunit;
using SeawispHunter.Common;

namespace test {

public class RandomFactoryTests {

  Random random;
  public RandomFactoryTests() {
    RandomFactory.SetSeed(0);
    random = RandomFactory.GetRandom();
  }

  [Fact]
  public void TestRandomFactory() {
    Assert.Equal(4, random.Next(-10, 10));
  }

  [Fact]
  public void TestRandomFactory2() {
    Assert.Equal(4, random.Next(-10, 10));
  }
}
}
