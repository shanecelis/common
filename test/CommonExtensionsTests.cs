// https://github.com/atomicobject/CSharpMemoizer/blob/master/Memoize/MemoizerTest.cs
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;
using SeawispHunter.Maths;

namespace SeawispHunter.Common.Tests {

public class CommonExtensionsTests {

  [Fact] public void TestRandomFromStream() {
    var a = Enumerable.Range(0, 5);
    var tally = new Tally<int>(5, x => x);
    for (int i = 0; i < 1000; i++)
      tally.Add(a.RandomFromStream(100));
    Assert.Equal(1.0 / 5.0, tally.probability[0], 1);
    Assert.Equal(1.0 / 5.0, tally.probability[1], 1);
    Assert.Equal(1.0 / 5.0, tally.probability[2], 1);
    Assert.Equal(1.0 / 5.0, tally.probability[3], 1);
    Assert.Equal(1.0 / 5.0, tally.probability[4], 1);
  }
}

}
