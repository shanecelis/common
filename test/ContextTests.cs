
using System;
using System.Linq;
using System.Collections.Generic;
using Xunit;
using System.Threading;
using SeawispHunter.Common;

public class ContextTests {

  [Fact] public void TestThreadLocalNoTracking() {
    var local = new ThreadLocal<int>(false);
    Assert.Throws<InvalidOperationException>(() => {var x = local.Values; });
    Assert.False(local.IsValueCreated);
    local.Value = 0;
    Assert.True(local.IsValueCreated);
    Assert.Throws<InvalidOperationException>(() => {var x = local.Values; });
    Assert.Equal(0, local.Value);
  }

  [Fact] public void TestThreadLocalTracking() {
    var local = new ThreadLocal<int>(true);
    Assert.Equal(new int[] { }, local.Values);
    Assert.False(local.IsValueCreated);
    local.Value = 0;
    Assert.True(local.IsValueCreated);
    Assert.Equal(new [] { 0 }, local.Values);
    Assert.Equal(0, local.Value);
  }

  [Fact] public void TestWithRunningThread() {
    var local = new ThreadLocal<int>(true);
    var thread = new Thread(() => {
        local.Value = 1;
      });
    thread.Start();
    thread.Join();
    Assert.Equal(new int[] { 1 }, local.Values);
    Assert.False(local.IsValueCreated);
    local.Value = 0;
    Assert.True(local.IsValueCreated);
    Assert.Equal(new [] { 0, 1 }, local.Values);
    Assert.Equal(0, local.Value);
  }

  [Fact] public void TestIsValueCreated() {
    var local = new ThreadLocal<int>(true);
    bool threadResult = false;
    var thread = new Thread(() => {
        threadResult = local.IsValueCreated;
      });
    thread.Start();
    thread.Join();
    Assert.False(local.IsValueCreated);
    Assert.False(threadResult);
  }

  [Fact] public void TestIsValueCreated2() {
    var local = new ThreadLocal<int>(true);
    local.Value = 1;
    bool threadCreated = false;
    bool threadCreatedPost = false;
    int threadValue = -1;
    var thread = new Thread(() => {
        threadCreated = local.IsValueCreated;
        threadValue = local.Value;
        threadCreatedPost = local.IsValueCreated;
      });
    thread.Start();
    thread.Join();
    Assert.True(local.IsValueCreated);
    Assert.False(threadCreated);
    Assert.Equal(0, threadValue);
    Assert.True(threadCreatedPost);
  }

  [Fact] public void TestIsValueCreated3() {
    var local = new ThreadLocal<int>(() => 2, true);
    local.Value = 1;
    bool threadCreated = false;
    bool threadCreatedPost = false;
    int threadValue = -1;
    var thread = new Thread(() => {
        threadCreated = local.IsValueCreated;
        threadValue = local.Value;
        threadCreatedPost = local.IsValueCreated;
      });
    thread.Start();
    thread.Join();
    Assert.True(local.IsValueCreated);
    Assert.False(threadCreated);
    Assert.Equal(2, threadValue);
    Assert.True(threadCreatedPost);
  }

  [Fact] public void TestDefaultValue() {
    var local = new ThreadLocal<string>(true);
    Assert.Null(local.Value);
    string s = null;
    var thread = new Thread(() => {
        s = local.Value;
      });
    thread.Start();
    thread.Join();
    Assert.Null(s);
    Assert.Null(local.Value);
  }

  [Fact] public void TestDefaultValue2() {
    var local = new ThreadLocal<string>(true);
    string s = null;
    var thread = new Thread(() => {
        local.Value = "hi";
        s = local.Value;
      });
    thread.Start();
    thread.Join();
    Assert.Null(local.Value);
    Assert.Equal("hi", s);
  }

  [Fact] public void TestWithRunningThread2() {
    var local = new ThreadLocal<int>(true);
    Assert.Equal(new int[] { }, local.Values);
    Assert.False(local.IsValueCreated);
    local.Value = 0;
    int threadValue = -1;
    var thread = new Thread(() => {
        threadValue = local.Value;
      });
    thread.Start();
    thread.Join();
    Assert.True(local.IsValueCreated);
    Assert.Equal(new [] { 0, 0 }, local.Values);
    Assert.Equal(0, local.Value);
    Assert.Equal(0, threadValue);
  }

  [Fact] public void TestWithRunningThread3() {
    var local = new ThreadLocal<int>(true);
    Assert.Equal(new int[] { }, local.Values);
    Assert.False(local.IsValueCreated);
    local.Value = 0;
    int threadValue = -1;
    var thread = new Thread(() => {
        threadValue = local.Value;
        local.Value = 1;
      });
    thread.Start();
    thread.Join();
    Assert.True(local.IsValueCreated);
    Assert.Equal(new [] { 1, 0 }, local.Values);
    Assert.Equal(0, local.Value);
    Assert.Equal(0, threadValue);
  }

  [Fact] public void TestContextValue() {
    var context = new Context<int>(0);
    Assert.Equal(0, context.Value);
    int threadValue = -1;
    var thread = new Thread(() => {
        threadValue = context.Value;
      });
    thread.Start();
    thread.Join();
    Assert.Equal(0, context.Value);
  }

  [Fact] public void TestContextSet() {
    var context = new Context<int>(0);
    Assert.Equal(0, context.Value);
    int threadValue = -1;
    var thread = new Thread(() => {
        threadValue = context.Value;
        context.Value = (1);
      });
    thread.Start();
    thread.Join();
    Assert.Equal(0, threadValue);
    Assert.Equal(1, context.Value);
  }

  [Fact] public void TestContextSet2() {
    var context = new Context<int>(0);
    Assert.Equal(0, context.Value);
    context.Value = (2);
    Assert.Equal(2, context.Value);
    int threadValue = -1;
    var thread = new Thread(() => {
        threadValue = context.Value;
        context.Value = (1);
      });
    thread.Start();
    thread.Join();
    Assert.Equal(2, threadValue);
    // Surprised this change is persisted between threads.
    Assert.Equal(1, context.Value);
  }

  // This was from the old version of R Context<T>.With<R>(T x, Func<T,R> f);
  // [Fact] public void TestContextSet3() {
  //   var context = new Context<int>(0);
  //   Assert.Equal(0, context.Value);
  //   context.Value = (2);
  //   Assert.Equal(2, context.Value);
  //   int threadValue = -1;
  //   var thread = new Thread(() => {
  //       var s = context.With(1, (x) => {
  //           threadValue = context.Value;
  //           return "hi";
  //         });
  //       // context.Value = (1);
  //     });
  //   thread.Start();
  //   thread.Join();
  //   Assert.Equal(1, threadValue);
  //   Assert.Equal(2, context.Value);
  // }

  [Fact] public void TestContextSet4() {
    var context = new Context<int>(0);
    Assert.Equal(0, context.Value);
    context.Value = (2);
    Assert.Equal(2, context.Value);
    int threadValue = -1;
    var thread = new Thread(() => {
        using (context.With(1)) {
          threadValue = context.Value;
        }
      });
    thread.Start();
    thread.Join();
    Assert.Equal(1, threadValue);
    Assert.Equal(2, context.Value);
  }

  [Fact] public void TestContextSet5() {
    var context = new Context<int>(0);
    Assert.Equal(0, context.Value);
    context.Value = (2);
    Assert.Equal(2, context.Value);
    int threadValue = -1;
    var thread = new Thread(() => {
        var s = context.With(1);
        threadValue = context.Value;
      });
    thread.Start();
    thread.Join();
    Assert.Equal(1, threadValue);
    Assert.Equal(2, context.Value);
  }

  [Fact] public void TestContextSet6() {
    var context = new Context<int>(0);
    Assert.Equal(0, context.Value);
    context.Value = (2);
    Assert.Equal(2, context.Value);
    int threadValue = -1;
    int threadValuePost = -1;
    IDisposable s = null;
    var thread = new Thread(() => {
        threadValue = context.Value;
        s = context.With(1);
        threadValuePost = context.Value;
      });
    thread.Start();
    thread.Join();
    Assert.Equal(2, threadValue);
    Assert.Equal(1, threadValuePost);
    Assert.Equal(2, context.Value);
    Assert.NotNull(s);
    s.Dispose();
    // It really seems like this should be left at 1.
    Assert.Equal(2, context.Value);
  }

  [Fact] public void TestContextSet7() {
    var context = new Context<int>(0);
    Assert.Equal(0, context.Value);
    context.Value = (2);
    Assert.Equal(2, context.Value);
    int threadValue = -1;
    var thread = new Thread(() => {
        threadValue = context.Value;
        // SetLocal only stays with this thread.
        context.LocalValue = (1);
      });
    thread.Start();
    thread.Join();
    Assert.Equal(2, threadValue);
    Assert.Equal(2, context.Value);
  }

  [Fact] void TestRandomExample() {
    var context = new Context<Random>(new Random());
    int threadOutput = -1;
    var thread = new Thread(() => {
        // Let's set the seed for this thread.
        using(context.With(new Random(0))) {

          var random = context.Value; // Not really impressive here, but if this
                                      // was done ten method calls in, you'd see
                                      // the value.
          threadOutput = random.Next(100);
        }
      });
    thread.Start();
    thread.Join();
    Assert.Equal(72, threadOutput); // new Random(0).Next(100) -> 72 every time.
  }

  static Context<Random> randomContext;

  [Fact] void TestRandomExampleCall() {
    randomContext = new Context<Random>(new Random());
    int threadOutput = -1;
    var thread = new Thread(() => {
        // Let's set the seed for this thread.
        using(randomContext.With(new Random(0))) {
          threadOutput = RandomNext(100);
        }
      });
    thread.Start();
    thread.Join();
    Assert.Equal(72, threadOutput); // new Random(0).Next(100) -> 72 every time.
  }

  static int RandomNext(int max) {
    return randomContext.Value.Next(max);
  }

  // A contrived example of how this Context class should work. Context provides
  // a thread-safe global or a thread-local context to store a value.
  //
  // A more realistic example might be for controlling Random class instances on
  // a per thread basis, but that's a little more confusing to follow.

  // A context object is available somewhere.
  static Context<int> intContext;

  // Imagine this method was called 10 levels deep, and you didn't want to pass
  // the context's value in each case, that's the real use case.
  static int GetValueFromIntContext() {
    return intContext.Value;
  }

  // Let's exercise this context object.
  [Fact] void TestExampleCall() {
    // Setup the global default to be 0.
    intContext = new Context<int>(0);
    int threadOutput = -1;
    int threadOutputPost = -1;
    var thread = new Thread(() => {
        // Setup the context for this thread to be 1.
        using (intContext.With(1)) {
          threadOutput = GetValueFromIntContext();
        }
        // Context reverts to the global value for thread after using() block.
        threadOutputPost = GetValueFromIntContext();
      });
    thread.Start();
    // Global context doesn't change even if the thread changes it.
    Assert.Equal(0, intContext.Value);
    thread.Join();
    // After the thread's done. Check its values.
    Assert.Equal(1, threadOutput);
    Assert.Equal(0, threadOutputPost);
    // Global context doesn't change despite thread's value changing.
    Assert.Equal(0, intContext.Value);
  }


}
