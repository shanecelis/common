﻿using System;

namespace SeawispHunter.Common {

public static class RandomFactory {

  // Let someone set this if they like.
  // private static Random _random = null;
  private static readonly Context<Random> context = new Context<Random>(new Random());

  /** The default initial behavior is to return a new Random instance and return
      the same instance thereafter.

      However, it returns a different instance if someone sets the seed. */
  public static Random GetRandom() {
    return context.Value;
  }

  /** Set the seed by setting up a new random instance. */
  public static void SetSeed(int seed) {
    // _random = new Random(seed);
    context.Value = new Random(seed);
  }

  public static void SetSeedLocal(int seed) {
    // _random = new Random(seed);
    context.LocalValue = new Random(seed);
  }
}

}
